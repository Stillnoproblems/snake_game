// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT_Barrier_generated_h
#error "Barrier.generated.h already included, missing '#pragma once' in Barrier.h"
#endif
#define MYPROJECT_Barrier_generated_h

#define MyProject_Source_MyProject_Barrier_h_13_SPARSE_DATA
#define MyProject_Source_MyProject_Barrier_h_13_RPC_WRAPPERS
#define MyProject_Source_MyProject_Barrier_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define MyProject_Source_MyProject_Barrier_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABarrier(); \
	friend struct Z_Construct_UClass_ABarrier_Statics; \
public: \
	DECLARE_CLASS(ABarrier, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ABarrier) \
	virtual UObject* _getUObject() const override { return const_cast<ABarrier*>(this); }


#define MyProject_Source_MyProject_Barrier_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABarrier(); \
	friend struct Z_Construct_UClass_ABarrier_Statics; \
public: \
	DECLARE_CLASS(ABarrier, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject"), NO_API) \
	DECLARE_SERIALIZER(ABarrier) \
	virtual UObject* _getUObject() const override { return const_cast<ABarrier*>(this); }


#define MyProject_Source_MyProject_Barrier_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABarrier(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABarrier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABarrier); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABarrier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABarrier(ABarrier&&); \
	NO_API ABarrier(const ABarrier&); \
public:


#define MyProject_Source_MyProject_Barrier_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABarrier(ABarrier&&); \
	NO_API ABarrier(const ABarrier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABarrier); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABarrier); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABarrier)


#define MyProject_Source_MyProject_Barrier_h_13_PRIVATE_PROPERTY_OFFSET
#define MyProject_Source_MyProject_Barrier_h_10_PROLOG
#define MyProject_Source_MyProject_Barrier_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_Barrier_h_13_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_Barrier_h_13_SPARSE_DATA \
	MyProject_Source_MyProject_Barrier_h_13_RPC_WRAPPERS \
	MyProject_Source_MyProject_Barrier_h_13_INCLASS \
	MyProject_Source_MyProject_Barrier_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject_Source_MyProject_Barrier_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject_Source_MyProject_Barrier_h_13_PRIVATE_PROPERTY_OFFSET \
	MyProject_Source_MyProject_Barrier_h_13_SPARSE_DATA \
	MyProject_Source_MyProject_Barrier_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject_Source_MyProject_Barrier_h_13_INCLASS_NO_PURE_DECLS \
	MyProject_Source_MyProject_Barrier_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT_API UClass* StaticClass<class ABarrier>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject_Source_MyProject_Barrier_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
