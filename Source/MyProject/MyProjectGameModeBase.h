
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyProjectGameModeBase.generated.h"

class AFood;


UCLASS()
class MYPROJECT_API AMyProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	AMyProjectGameModeBase();

	UPROPERTY(BlueprintReadWrite)
		AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(EditDefaultsOnly)
		float FoodTime;

	
protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	void FoodSpawn();


};
