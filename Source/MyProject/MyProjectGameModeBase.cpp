

#include "MyProjectGameModeBase.h"
#include "Food.h"

AMyProjectGameModeBase::AMyProjectGameModeBase()
{

	PrimaryActorTick.bCanEverTick = true;
	FoodTime = 4.f;
	

}

void AMyProjectGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(FoodTime);

	FoodSpawn();

}


void AMyProjectGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyProjectGameModeBase::FoodSpawn()
{	
	float XValue = rand() % 1000;
	float YValue = rand() % 1000;


	

	FVector NewPosition(XValue, YValue, 0);

	
	
	FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, FTransform(NewPosition));
}
