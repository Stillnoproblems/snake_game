

#include "Barrier.h"
#include "SnakeBase.h"


ABarrier::ABarrier()
{
 	
	PrimaryActorTick.bCanEverTick = true;

}


void ABarrier::BeginPlay()
{
	Super::BeginPlay();
	
}


void ABarrier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABarrier::Interact(AActor* Interactor, bool bIsHead)
{
	ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);
	
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

