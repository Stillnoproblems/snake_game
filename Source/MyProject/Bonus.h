
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Bonus.generated.h"

UCLASS()
class MYPROJECT_API ABonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	
	ABonus();

protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;


};
