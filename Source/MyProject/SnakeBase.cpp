
#include "SnakeBase.h"
#include "SnakeElementBase.h" 
#include "Interactable.h"






ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 200.f;
	Interval = 0.5f;
	LastMoveDirection = EMovementDirection::E_DOWN;

}


void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	AddSnakeElement();
	
	
}


void ASnakeBase::Tick(float DeltaTime)
{	
	SetActorTickInterval(Interval);

	Super::Tick(DeltaTime);
	
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{	
	for (int i = 0; i < ElementsNum; i++)
	{
	
		
		FVector NewLocation(ForceInitToZero);

		int32 ElemIndex = 0;

		if (SnakeElements.Num() == 0)
		{
			FVector AnotherElementsLocation(640, 640, 0);
			NewLocation = AnotherElementsLocation;
			FTransform NewTransform = FTransform(NewLocation);
			NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		}
		else
		{
			FVector AnotherElementsLocation(/*ElementSize*SnakeElements.Num()*/ -1000, 0, 0);
			NewLocation = AnotherElementsLocation;
			FTransform NewTransform = FTransform(NewLocation);
			NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);


		}
		
				
			
		NewSnakeElement->SnakeOwner = this;
		
		
		ElemIndex = SnakeElements.Add(NewSnakeElement);

		
		
		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MoveSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::E_UP:
		MovementVector.X += MoveSpeed;
		break;
	case EMovementDirection::E_DOWN:
		MovementVector.X -= MoveSpeed;
		break;
	case EMovementDirection::E_RIGHT:
		MovementVector.Y += MoveSpeed;
		break;
	case EMovementDirection::E_LEFT:
		MovementVector.Y -= MoveSpeed;
		break;




	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];

		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		
		
		//CurrentElement->SetActorHiddenInGame(false);
	}
	
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	//SnakeElements[0]->SetActorHiddenInGame(false);

	
	
	SnakeElements[0]->ToggleCollision();
	

	
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{	
		
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	
	}	

}


 




