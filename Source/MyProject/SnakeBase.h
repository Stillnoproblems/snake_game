
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;

UENUM()
enum class EMovementDirection
{
	E_UP,
	E_DOWN,
	E_LEFT,
	E_RIGHT

};

UCLASS()
class MYPROJECT_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(BlueprintReadOnly)
	ASnakeElementBase* NewSnakeElement;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	

	UPROPERTY(EditDefaultsOnly)
	float Interval;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;


protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor);

};
