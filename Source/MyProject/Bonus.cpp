#include "SnakeBase.h"
#include "Bonus.h"


ABonus::ABonus()
{
 	
	PrimaryActorTick.bCanEverTick = true;

}


void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}


void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonus::Interact(AActor* Interactor, bool bIsHead)
{
	ASnakeBase* Snake = Cast<ASnakeBase>(Interactor);

	if (IsValid(Snake))
	{
		Snake->Interval *= 0.5;
		Destroy();
	}

}

